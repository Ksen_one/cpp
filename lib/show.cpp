#include <stdio.h>
#include "show.h"

void show(char name[], char group[]) {
    printf("Name: %s. Group: %s.\n", name, group);
}