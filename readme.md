### Компилируем код библиотеки
```
gcc -c lib/show.cpp -o lib/show.o
```
### Собираем динамическую библиотеку (so - тот же dll)
```
gcc -shared -o dist/libshow.so lib/show.o
```
### Компилируем программу, которая использует dll
```
gcc -o dist/main.exe src/main.cpp dist/libshow.so
```
### Запускаем main.exe